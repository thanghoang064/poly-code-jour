const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);

const memberList = $('.register .members');
const btn_add = $('.btn-add');
let count = 1;
function addnewItem(){
    const a = document.createElement("div");
    a.innerHTML = `<div class="row register-bottom align-items-md-center mt-4 remove">
    <div class="col-md-2 px-0 text-center">
        <h3 class="my-0 px-0">Thành viên ${++count}</h3>
    </div>

    <div class="col-md-2">
        <div class="row align-items-md-center ">
            <div class="col-md-4 px-0 text-center">
                <h4 class="my-0" >Mã Sv</h4>
            </div>
            <div class="col-md-8 px-0">
                <input type="text" class="form-control" name="maSVTV${count++}" value="" />
            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="row align-items-md-center">
            <div class="col-md-4 px-0 text-center">
                <h4 class="my-0">Tên sv</h4>
            </div>
            <div class="col-md-8 px-0">
                <input type="text" class="form-control" name="tenSVTV${count++}" value="" />
            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="row align-items-md-center">
            <div class="col-md-4 px-0 text-center">
                <h4 class="my-0">Kì</h4>
            </div>
            <div class="col-md-8 px-0">
                <select class="form-control" name="kiSVTV${count++}">
                                    <option class="hidden"  selected disabled>Chọn Kì</option>
                                    <option value="1">Kì 1</option>
                                    <option value="2">Kì 2</option>
                                    <option value="3">Kì 3</option>
                                    <option value="4">Kì 4</option>
                                    <option value="5">Kì 5</option>
                                    <option value="6">Kì 6</option>
                                    <option value="7">Kì 7</option>
                </select>
            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="row align-items-md-center">
            <div class="col-md-4 px-0 text-center">
                <h4 class="my-0">SDT</h4>
            </div>
            <div class="col-md-8 px-0">
                <input type="text" class="form-control" name="sdtSVTV${count++}" value="" />
            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="row align-items-md-center">
            <div class="col-md-4 px-0 text-center">
                <h4 class="my-0">Email</h4>
            </div>
            <div class="col-md-6 px-0">
                <input type="email" class="form-control" name="emailSVTV${count++}" value="" />
            </div>

            <div class="col-md-2">
                <button type="button" class="btn-remove" onclick="remove()" ><i class="fa-solid fa-dash"></i></button>
            </div>

        </div>
    </div>
</div>`
    memberList.appendChild(a);
}
btn_add.onclick = () => {
    addnewItem();
}

function remove() {
    const e = $$('.register-bottom.remove');
    e[e.length -1].remove();
    count--;
}







