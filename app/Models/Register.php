<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class Register extends Model
{
    use HasFactory;
    protected $table = "registers";
    protected $fillable = [];

    public function loadListWithPager($params = []) {
        $query = DB::table($this->table)
            ->select($this->fillable);
        $lists = $query->paginate(10);
        return $lists;
    }
    public function saveRegister($params) {
//        $data = array_merge($params['cols'],[
//                'password' => Hash::make($params['cols']['password']),
//                'level'=>1,
//            ]
//        );
      //  $res = DB::table($this->table)->insertGetId($data);
        return $res;
    }

    public function loadOne($id, $param = null) {
        $query = DB::table($this->table)
            ->where('id','=',$id);
        $obj = $query->first();
        return $obj;
    }
    public function saveUpdate($params) {
        if (empty($params['cols']['id'])) {
            Session::flash('error','Khong xac dinh ban ghi cap nhat');
            return null;
        }
        $dataUpdate = [];
        foreach ($params['cols'] as $colName=> $val) {
            if ($colName == 'id') continue;
            if(in_array($colName,$this->fillable)) {
                $dataUpdate[$colName] = (strlen($val) == 0) ? null:$val;
            }
        }
        $res = DB::table($this->table)
            ->where('id',$params['cols']['id'])
            ->update($dataUpdate);
        return $res;

    }



}
