<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Socialite;
use App\Models\User;
use SheetDB\SheetDB;
use GuzzleHttp\Client;
class LoginController extends Controller
{
    public function redirectToProvider($driver)
    {
//        dd($driver);
        return Socialite::driver($driver)->redirect();
    }

    public function handleProviderCallback()
    {
        $driver = "google";

        try {
            $user = Socialite::driver($driver)->user();

        } catch (\Exception $e) {
            return redirect()->route('login');
        }

        $existingUser = User::where('email', $user->getEmail())->first();

        if ($existingUser) {

            auth()->login($existingUser, true);
        } else {
            $newUser                    = new User;
            $newUser->provider_name     = $driver;
            $newUser->provider_id       = $user->getId();
            $newUser->name              = $user->getName();
            $newUser->email             = $user->getEmail();
            $newUser->email_verified_at = now();
            $newUser->password = 123;
            $newUser->avatar            = $user->getAvatar();
            $newUser->save();

            auth()->login($newUser, true);
        }
//        dd(Auth::user());

        return redirect('/register/add');
    }

//    public function loginCallback(Request $request)
//    {
//        try {
//            $state = $request->input('state');
//
//            parse_str($state, $result);
//            $googleUser = Socialite::driver('google')->stateless()->user();
//
//            $user = User::where('email', $googleUser->email)->first();
//            if ($user) {
//                throw new \Exception(__('google sign in email existed'));
//            }
//            $user = User::create(
//                [
//                    'email' => $googleUser->email,
//                    'name' => $googleUser->name,
//                    'google_id'=> $googleUser->id,
//                    'password'=> '123',
//                ]
//            );
//            return response()->json([
//                'status' => __('google sign in successful'),
//                'data' => $user,
//            ], Response::HTTP_CREATED);
//
//        } catch (\Exception $exception) {
//            return response()->json([
//                'status' => __('google sign in failed'),
//                'error' => $exception,
//                'message' => $exception->getMessage()
//            ], Response::HTTP_BAD_REQUEST);
//        }
//    }
    public function getLogin() {
//        $sheetDB = new SheetDB('nrobbk6rs9die');
////        $response = $sheetDB->get();
////        $sheetDB->create(['name'=>'Mark','age'=>'35']);
//        $sheetDB->create([
//            ['name'=>'Chris','age'=>'34'],
//            ['name'=>'Amanda','age'=>'29'],
//        ]);

//        $url = "https://script.google.com/macros/s/AKfycbwGy6ofQaW5yU2lPD-rL08X5Ax4a2xcMvPp2UxcxmMAz4HQKIohlPKb39su9BBL-w5E/exec";
//        $client = new Client();
//        $headers = [
//            'Accept' => 'application/json',
//        ];
//        $requestLog = $client->get($url, [
//            'headers' => $headers
//        ]);
//        $data = json_decode($requestLog->getBody()->getContents());
//        dd($data);
//        $res = $client->post($url, [
//            'form_params' => [
//                'username' => "123123",
//                'password' => "1232131"
//            ],
//            'headers' => $headers
//        ]);
//
//        $tokenService = json_decode($res->getBody()->getContents());
//        dd($tokenService);
        return view('auth.login');
    }
    public function logOut() {
        Auth::logout();
        return redirect('/home');
    }
}
