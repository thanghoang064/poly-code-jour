<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $v;

    public function __construct()
    {
        $this->v = [];
    }

    public function index() {
        return view('home.index');
    }
    public function register() {
        return view('register.index');
    }
    public function  add(Request $request) {
        if ($request->isMethod('post')) {
            $fillable = ['masv','sdt','tensv','doi','ki'];
            $dataRequest = $request->all();
            unset($dataRequest['_token']);
            $dataLead = [];
            $dataMemBer = [];
            foreach ($dataRequest as $colName=> $val) {
                if(in_array($colName,$fillable)) {
                    $dataLead[$colName] = (strlen($val) == 0) ? null:$val;
                } else {
                    $dataMemBer[$colName] = (strlen($val) == 0) ? null:$val;
                }
            }

            $dataMemBerChunk =  array_chunk($dataMemBer,5);
            $dataMemBerNew = [];

//            dd($dataMemBerChunk);
            foreach ($dataMemBerChunk as $key=>$value) {
                $dataMemBerNew[$key]['code'] = $value[0];
                $dataMemBerNew[$key]['name'] = $value[1];
                $dataMemBerNew[$key]['email'] = $value[4];
                $dataMemBerNew[$key]['id_group'] = 1;
                $dataMemBerNew[$key]['id_table'] = 1;
                $dataMemBerNew[$key]['id_schedule'] = 1;
                $dataMemBerNew[$key]['id_role'] = 1;
                $dataMemBerNew[$key]['created_at'] = date('Y-m-d H:i:s');
                $dataMemBerNew[$key]['updated_at']  = date('Y-m-d H:i:s');
            }
            dd($dataLead);


        }
        $this->v['hoc_ki'] = config('app.hoc_ki');

        return view('register.add',$this->v);
    }
}
