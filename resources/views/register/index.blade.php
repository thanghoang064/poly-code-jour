@extends('template_frontend.layout')
@section('content')
    <section class="contact-section" id="contact-section" style="padding: 100px 0px 350px!important;">
        <div
            class="bg-pattern"
            style="background-image: url('images/background/pattern-2.png')"
        ></div>
        <div class="auto-container">
            <div class="title-box-two title-box-two-custom centered">
                <h2>Đăng nhập để đăng ký đội thi</h2>
            </div>
            <div class="info-box">
                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                    <div class="text-center">
                        <button class="theme-btn btn-style-one" onclick="location.href='{{ route('login.provider','google') }}'">
                            <div class="text-center">
                                <img width="20px" style="margin-top:7px; margin-right:8px" alt="Google sign-in"
                                     src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png" />
                            </div>
                            <span class="btn-title">Đăng nhập với google</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
