@extends('template_frontend.layout')
@section('content')
    <section class="contact-section" id="contact-section">
        <div class="container register">
            <form method="POST" action="">
                @csrf
            <div class="row">
                <div class="col-md-3 register-left">
                    <img src="https://caodang.fpt.edu.vn/wp-content/uploads/logo-3.png" alt=""/>
                    <h3>Welcome</h3>
                    <p>Nhanh tay đăng kí thành viên để cùng đứng trên đỉnh vinh quang nào</p>
                </div>
                <div class="col-md-9 register-right">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <h3 class="register-heading text-dark">Nhanh Tay đăng kí nào !</h3>
                            <div class="row register-form">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="masv" placeholder="Mã sinh viên *" value="" />
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option class="hidden"  selected disabled>Chọn bảng</option>
                                            <option value="1">Bảng A</option>
                                            <option value="2">Bảng B</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="sdt" placeholder="Số điện thoại *" value="" />
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Tên Sinh viên *" name="tensv" value="" />
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" name="doi">
                                            <option class="hidden"  selected disabled>Chọn đội</option>
                                            <option value="1">Đội A</option>
                                            <option value="2">Đội B</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" name="ki">

                                            <option class="hidden"  selected disabled>Chọn Kì</option>
                                            @foreach($hoc_ki as $key=>$value)
                                                 <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="members">
                <div class="row register-bottom align-items-md-center mt-4">
                    <div class="col-md-2 px-0 text-center">
                        <h3 class="my-0 px-0">Thành viên 1</h3>
                    </div>

                    <div class="col-md-2">
                        <div class="row align-items-md-center ">
                            <div class="col-md-4 px-0 text-center">
                                <h4 class="my-0" >Mã Sv</h4>
                            </div>
                            <div class="col-md-8 px-0">
                                <input type="text" name="maSVTV1" class="form-control" value="" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="row align-items-md-center">
                            <div class="col-md-4 px-0 text-center">
                                <h4 class="my-0">Tên sv</h4>
                            </div>
                            <div class="col-md-8 px-0">
                                <input type="text" name="tenSVTV1" class="form-control" value="" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="row align-items-md-center">
                            <div class="col-md-4 px-0 text-center">
                                <h4 class="my-0">Kì</h4>
                            </div>
                            <div class="col-md-8 px-0">
                                <select class="form-control" name="kiSVTV1">
                                    <option class="hidden"  selected disabled>Chọn Kì</option>
                                    @foreach($hoc_ki as $key=>$value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="row align-items-md-center">
                            <div class="col-md-4 px-0 text-center">
                                <h4 class="my-0">SDT</h4>
                            </div>
                            <div class="col-md-8 px-0">
                                <input type="text" class="form-control" name="sdtSVTV1" value="" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="row align-items-md-center">
                            <div class="col-md-4 px-0 text-center">
                                <h4 class="my-0">Email</h4>
                            </div>
                            <div class="col-md-6 px-0">
                                <input type="email" class="form-control" name="emailSVTV1" value="" />
                            </div>

                            <div class="col-md-2">
                                <button type="button" class="btn-add" ><i class="fa-solid fa-plus"></i></button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="regiter">
                <input type="submit" class="btnRegister"  value="Đăng kí"/>
            </div>
            </form>
        </div>
    </section>
@endsection
