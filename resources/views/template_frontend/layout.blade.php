@php
$objUser = \Illuminate\Support\Facades\Auth::user();
@endphp
<!DOCTYPE html>

<html lang="en">
<!-- Mirrored from demo.ovathemes.com/cryptlight_html/cryplight/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 14 Jun 2022 12:21:00 GMT -->
<head>
    <meta charset="utf-8" />
    <title>CryptLight - Home 01</title>
    <!-- Stylesheets -->
    <link href="{{ asset('front/css/bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('front/css/style.css') }}" rel="stylesheet" />
    <link rel="shortcut icon" href="./images/favicon_io/favicon-16x16.png" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="180x180" href="./images/favicon_io/apple-touch-icon.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="./images/favicon_io/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon_io/favicon-16x16.png" />
    <link rel="manifest" href="./images/favicon_io/site.webmanifest" />
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
    />
    <link href="{{ asset('front/css/responsive.css') }} " rel="stylesheet" />
    <!--[if lt IE 9
            ]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script
    ><![endif]-->
</head>

<body class="home-two home-custom dark-view">
<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Main Header-->
    <header class="main-header header-one">
        <!-- Header Upper -->
        <div class="header-upper">
            <div class="auto-container">
                <!-- Main Box -->
                <div class="main-box clearfix">
                    <!--Logo-->
                    <div class="logo-box logo-box-custom clearfix">
                        <div class="logo">
                            <a href="index.html" title="CryptLight"
                            ><img
                                    src="https://caodang.fpt.edu.vn/wp-content/uploads/logo-3.png"
                                    alt=""
                                    title="CryptLight"
                                /></a>
                        </div>
                    </div>

                    <div class="nav-box clearfix">
                        <!--Nav Outer-->
                        <div class="nav-outer clearfix">
                            <nav class="main-menu">
                                <ul class="navigation clearfix" id="scroll-nav">
                                    @if(empty($objUser))
                                    <li>
                                        <a href="#intro-section">Về cuộc thi</a>
                                    </li>
                                    <li>
                                        <a href="#criteria">Tiêu chí</a>
                                    </li>
                                    <li>
                                        <a href="#documents">Thể lệ</a>
                                    </li>
                                    <li>
                                        <a href="#how-it-works">Bảng thi</a>
                                    </li>
                                    <li>
                                        <a href="#prize">Giải thưởng</a>
                                    </li>

                                    <li>
                                        <a href="#road-map">Lịch trình</a>
                                    </li>
                                    <li>
                                        <a href="#contact-section">Liên hệ</a>
                                    </li>
                                    @endif
                                    @isset($objUser)
                                    <li>
                                        <a href="">Xin chào {{ $objUser->name }}</a>
                                    </li>
                                        <li>
                                            <a href="{{ route('logout') }}">Đăng xuất</a>
                                        </li>
                                    @endisset
                                </ul>
                            </nav>
                            <!-- Main Menu End-->
                        </div>
                        <!--Nav Outer End-->
                        <!-- Hidden Nav Toggler -->
                        <div class="nav-toggler">
                            <button class="hidden-bar-opener">
                                <span class="icon"><i class="fa-regular fa-bars"></i></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--End Main Header -->

    <!--Menu Backdrop-->
    <div class="menu-backdrop"></div>

    @yield('content')

    <!--Main Footer-->
    <footer class="main-footer">
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="inner">
                    <div class="copyright">
                        Copyright &copy; 2022. Bản quyền thuộc về <a href="https://caodang.fpt.edu.vn/">Trường Cao Đẳng FPT Polytechnic</a>.
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html">
            <span class="icon"
            ><img src="images/icons/arrow-up.svg" alt="" title="Go To Top"
                /></span>
</div>

<script
    data-cfasync="false"
    src="http://demo.ovathemes.com/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"
></script>

<script src="{{ asset('front/js/jquery.js') }}"></script>
<script src="{{ asset('front/js/popper.min.js') }}"></script>
<script src="{{ asset('front/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('front/js/PageScroll.js') }}"></script>
<script src="{{ asset('front/js/owl.js') }}"></script>
<script src="{{ asset('front/js/bxslider.js') }}"></script>
<script src="{{ asset('front/js/countdown.js') }}"></script>
<script src="{{ asset('front/js/jquery.fancybox.js') }}"></script>
<script src="{{ asset('front/js/jquery-ui.js') }}"></script>
<script src="{{ asset('front/js/appear.js') }}"></script>
<script src="{{ asset('front/js/wow.js') }}"></script>
<script src="{{ asset('front/js/custom-script.js') }}"></script>
<script src="{{ asset('front/js/add_member.js') }}"></script>
</body>

<!-- Mirrored from demo.ovathemes.com/cryptlight_html/cryplight/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 14 Jun 2022 12:23:08 GMT -->
</html>
