<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('redirect/{driver}', 'Auth\LoginController@redirectToProvider')
    ->name('login.provider')
    ->where('driver', implode('|', config('auth.socialite.drivers')));
//Route::get('/login',['as'=>'login','uses'=>'Auth\LoginController@getLogin']);
Route::get('/logout',['as'=>'logout','uses'=>'Auth\LoginController@logOut']);

Route::get('/home', 'HomeController@index');
Route::get('/register', 'HomeController@register')->name('login');
Route::middleware(['auth'])->group(function (){
    Route::match(['get','post'],'/register/add','HomeController@add');
//    Route::get('/register/add', 'HomeController@add');
});
Route::get('google/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('google/callback', 'Auth\LoginController@handleProviderCallback');
//    ->where('driver', implode('|', config('auth.socialite.drivers')));
